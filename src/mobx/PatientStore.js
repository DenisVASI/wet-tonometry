import {action, observable} from "mobx";

class PatientStore {

    //сюда сохраняем данные с формы, которая содержит информацию о пациенте
    @observable patientForm = null;

    @action
    setPatientForm(values, callback) {
        this.patientForm = values;
        if (typeof callback === "function") callback()
    }
}

export default PatientStore;