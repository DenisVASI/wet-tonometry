import {action, observable} from "mobx";

class AuthStore {

    @observable authData = null;

    @action
    exit(callback) {
        this.authData = null;
        if (typeof callback === "function") {
            callback();
        }
    }

    @action
    setAuthData(auth, callback) {
        this.authData = auth;
        if (typeof callback === "function") {
            callback();
        }
    }
}

export default AuthStore;