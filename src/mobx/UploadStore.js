import {action, observable} from "mobx";
import axios from "axios";
import config from "../config/production";
import ImageProcessing from "../components/ImageProcessing";

class UploadStore {


    //файл заруденный со страницы загрузки фотографии
    @observable file = null;

    @action
    setFile(file) {
        this.file = file;
    }

    //файл переведенный в base64
    @observable base64file = null;

    @action
    setFileBase64(base64file) {
        this.base64file = base64file;
    }

    //файл полученный после обрезки в cropperJS
    @observable croppedFile = null;

    @action
    setCroppedFile(file) {
        this.croppedFile = file;
    }

    //файл обрезанный по координатам пришедшим после обработки фото детектором
    @observable imageFromCanvas = null;

    @action
    setImageFromCanvas(image) {
        this.imageFromCanvas = image;
    }

    //избражение пришедшее от детектора
    @observable savedImage = null;

    //изоражение на которое была налодена белая рамка с черной грацей(для детектора)
    @observable processedImage = null;

    //ответ от нейронки которая измеряет круги
    @observable uploadResponse = null;

    //отправка изобрадения в детектор
    @action
    sendCroppedFileToApi(file, callback, reject) {
        return new Promise(async resolve => {
            let data = new FormData();
            data.append("image", file);

            await axios.post(`${config.node_protocol}${config.node_url}:${config.node_port}/api/v1/images/draw`, data).then(async res => {

                this.processedImage = res.data;

                const binaryImage = ImageProcessing.Base64toBlob(res.data.base64);

                let formData = new FormData();
                formData.append("image", binaryImage);

                const response = await axios.post(`${config.protocol}${config.url}:${config.port}/circles`, formData)
                    .then(response => response.data)
                    .catch(e => {
                        reject();
                        return {
                            response: e,
                            error: 'Файл картинки не загружен на сервер'
                        }
                    });

                if (response.image_name) {
                    this.uploadResponse = response;
                    this.savedImage = `${config.protocol}${config.url}:${config.port}/images/${response.image_name}`;
                    if (typeof callback === "function") callback();
                }

            }).catch(e => {
                return {
                    response: e,
                    error: 'Файл картинки не загружен на сервер'
                }
            });
            return resolve();
        });
    }

}

export default UploadStore;