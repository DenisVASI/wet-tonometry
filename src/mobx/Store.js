import {action, observable} from "mobx";
import axios from "axios";
import config from "../config/production.json";


class Store {
    //язык
    @observable language = null;

    @action setLanguage(lang) {
        this.language = lang;
    }
    //язык

    @observable inputSelect = "";

    @observable status = false;
    @observable calValues;
    @observable calc;
    @observable authData = null;

    @action
    setAuthData(auth, callback) {
        this.authData = auth;
        if (typeof callback === "function") {
            callback();
        }
    }

    @action
    setStatus(status) {
        this.status = status;
    }







    @action
    setOption(option) {
        this.inputSelect = option;
    }

    //логика редиректов

    @observable selectedMethod = null;
    @observable result = null;
    @observable avL = null;
    @observable avR = null;


    @action
    setAVLeft(av) {
        this.avL = av;
    }

    @action
    setAVRight(av) {
        this.avR = av;
    }

    @action setMethod(method) {
        this.selectedMethod = method;
    }

    @action
    async getRecomendation(req_data, callback) {
        const response = await axios.post(`${config.protocol}${config.url}:${config.port}/recommender_system_maklakov`, req_data)
            .then(response => response.data)
            .catch(e => {
                return {
                    response: e,
                    error: 'Данные не были загружены в рекомендательную систему'
                }
            });
        this.result = response.recommendation;
        if (typeof callback === "function") callback();
    }

    @action
    SetCalculationValues(calValues) {
        this.calValues = calValues;
        //let CHOD = ( (0.149 * (Number(calValues["PccOD"]))) - Number(calValues["PgOD"]));
        //let CHOS = ( (0.149 * (Number(calValues["PccOS"]))) - Number(calValues["PgOS"]));
        let P2OD = ((4.04 * (Number(calValues["PgOD"]))) + (3.1 * (Number(calValues["PccOD"]))) + 24.85);
        let P2OS = ((4.04 * (Number(calValues["PgOS"]))) + (3.1 * (Number(calValues["PccOS"]))) + 24.85);
        let P1OD = ((9.38 * (Number(calValues["PgOD"]))) - (3.1 * (Number(calValues["PccOD"]))) + 111.2);
        let P1OS = ((9.38 * (Number(calValues["PgOS"]))) - (3.1 * (Number(calValues["PccOS"]))) + 111.2);
        let CHOD = (0.149 * (Number(P1OD) - Number(P2OD)));
        let CHOS = (0.149 * (Number(P1OS) - Number(P2OS)));
        let CRFOD = ((0.149 * (Number(P1OD) - (0.7 * Number(P2OD)))) - 6.12);
        let CRFOS = ((0.149 * (Number(P1OS) - (0.7 * Number(P2OS)))) - 6.12);
        let KbsOD = ((Number(calValues["PgOD"])) / (Number(CHOD) + Number(CRFOD)));
        let KbsOS = ((Number(calValues["PgOS"])) / (Number(CHOS) + Number(CRFOS)));

        //this.calValues.push(CHOD, CHOS, CRFOD, CRFOS, KbsOD, KbsOS);
        this.calc = ([calValues["PgOD"], calValues["PgOS"], calValues["PccOD"], calValues["PccOS"], CHOD, CHOS, CRFOD, CRFOS, KbsOD, KbsOS]);
    }

    // измерения с занесением в историю
    @observable patientForMeasurement = null;
    @observable masses = null;
    @observable pressures = null;


    @action
    setPatient(id, callback) {
        this.patientForMeasurement = id;
        if (typeof callback === "function") callback();
    }

    @action
    setMasses(masses) {
        this.masses = masses;
    }

    @action
    setPressures(pressures) {
        this.pressures = pressures;
    }

    //эласто
    @observable mode = null;
    @observable elastoData = {};

    @action
    setMode(mode, data) {
        this.mode = mode;
        if (data) {
            this.elastoData = data;
        }
    }
}

export default Store;

