import {action, observable} from "mobx";

class MainLayoutStore {

    //открыто ли меню с навигацией(справа)
    @observable showMenu = true;

    @action
    toggleMenu(params) {
        if (params) {

            if (
                (params.subAction && params.subAction === 'close_help') &&
                (params.width && params.width <= 1000)
            ) {
                this.showHelp = false;
            }
            this.showMenu = params.show;
        } else {
            this.showMenu = !this.showMenu;
        }
    }

    //открыто ли меню с помощью(слева)
    @observable showHelp = true;

    @action
    toggleHelp(params) {
        if (params) {

            if (
                (params.subAction && params.subAction === 'close_menu') &&
                (params.width && params.width <= 1000)
            )
                this.showMenu = false;

            this.showHelp = params.show;

        } else {
            this.showHelp = !this.showHelp;
        }
    }

    //ширина окна браузера
    @observable width = 0;

    @action
    setWidth(width) {
        this.width = width;
    }
}

export default MainLayoutStore;