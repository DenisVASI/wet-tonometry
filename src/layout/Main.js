import React from "react";
import {inject, observer} from "mobx-react";
import {NavLink} from "react-router-dom";
import Loader from "../components/Loader";
import HeaderMobile from "../components/Mobile/HeaderMobile";
import TnPageHeader from "../components/PageHeader";

@inject("mainLayoutStore")
@observer
class Main extends React.Component {

    constructor(props) {
        super(props);
        console.log('call main layout constructor');
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.getDimensions();

    }

    componentDidMount() {
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    getDimensions() {

        console.log('call getDimensions');


        const {mainLayoutStore} = this.props;

        mainLayoutStore.setWidth(window.innerWidth);

        if (window.innerWidth <= 1000) {
            mainLayoutStore.toggleHelp({
                show: false,
                width: window.innerWidth,
            });
            mainLayoutStore.toggleMenu({
                show: false,
                width: window.innerWidth,
            });
        }
    }

    updateWindowDimensions() {

        console.log('call updateWindowDimensions');

        const {mainLayoutStore} = this.props;

        mainLayoutStore.setWidth(window.innerWidth);

        const width = mainLayoutStore.width;

        if (width <= 1000) {
            mainLayoutStore.toggleHelp({
                show: false,
                width: width
            });
            mainLayoutStore.toggleMenu({
                show: false,
                width: width
            });
        } else {
            mainLayoutStore.toggleHelp({
                show: true,
                width: width
            });
            mainLayoutStore.toggleMenu({
                show: true,
                width: width
            });
        }
    }

    componentWillUnmount() {
        console.log('call componentWillUnmount');

        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    render() {

        const {content, rightbar, rightbarTitle, stage, mainLayoutStore} = this.props;
        let contentClass = "main_layout__content";

        if (!mainLayoutStore.showMenu) contentClass += " without_sidebar";
        if (!mainLayoutStore.showHelp) contentClass += " without_help";

        return (
            <div className="main_layout">
                {mainLayoutStore.width <= 420 && <HeaderMobile
                    actionMenu={() => {
                        console.log('showMenu');
                        mainLayoutStore.toggleMenu({
                            subAction: 'close_help',
                            width: mainLayoutStore.width,
                            show: !mainLayoutStore.showMenu
                        });
                    }}
                    actionHelp={() => {
                        mainLayoutStore.toggleHelp({
                            subAction: 'close_menu',
                            width: mainLayoutStore.width,
                            show: !mainLayoutStore.showHelp
                        })
                    }}/>}
                <div
                    className={mainLayoutStore.showMenu ? "main_layout__sidebar sidebar_show" : "main_layout__sidebar sidebar_hide"}>
                    <div className="main_layout__logo">
                        <h2>AI Tonometry</h2>
                    </div>

                    <div className="main_layout__menu">
                        {stage > 0 && <div className="main_layout__menu_item">
                            <NavLink exact={true} to="/" activeClassName="menu_item_active">Главная</NavLink>
                        </div>}

                        {/*{authData && <div className="main_layout__menu_item">*/}
                        {/*<NavLink to="/patients" activeClassName="menu_item_active">Пациенты</NavLink>*/}
                        {/*</div>}*/}

                        {stage > 1 && <div className="main_layout__menu_item">
                            <NavLink exact={true} to="/upload" activeClassName="menu_item_active">Загрузить</NavLink>
                        </div>}

                        {stage > 2 && <div className="main_layout__menu_item">
                            <NavLink exact={true} to="/patient" activeClassName="menu_item_active">О
                                пациенте</NavLink>
                        </div>}
                        {stage > 3 && <div className="main_layout__menu_item">
                            <NavLink to="/masses" activeClassName="menu_item_active">Массы</NavLink>
                        </div>}
                        {stage > 4 && <div className="main_layout__menu_item">
                            <NavLink to="/result" activeClassName="menu_item_active">Результат</NavLink>
                        </div>}

                        {/*{!authData && <div className="main_layout__menu_item">*/}
                        {/*<NavLink to="/login" activeClassName="menu_item_active">Авторизоваться</NavLink>*/}
                        {/*</div>}*/}
                        {/*<div className="main_layout__menu_item">*/}
                        {/*<NavLink to="/methodemaklakov" activeClassName="menu_item_active">Методика Тонометрии по Маклакову</NavLink>*/}
                        {/*</div>*/}
                        {/*<div className="main_layout__menu_item">*/}
                        {/*<NavLink to="/methodeelasto" activeClassName="menu_item_active">Методика Эластотонометрии</NavLink>*/}
                        {/*</div>*/}
                    </div>
                </div>
                <Loader/>
                <div className={contentClass}>
                    {(mainLayoutStore.showMenu || mainLayoutStore.showHelp) && <div className="overlay" onClick={() => {
                        mainLayoutStore.toggleMenu({
                            show: false
                        });
                        mainLayoutStore.toggleHelp({
                            show: false
                        });
                    }}/>}
                    <TnPageHeader
                        menuAction={() => {
                            mainLayoutStore.toggleMenu({
                                subAction: 'close_help',
                                width: mainLayoutStore.width,
                                show: !mainLayoutStore.showMenu
                            });
                        }}
                        helpAction={() => {
                            mainLayoutStore.toggleHelp({
                                subAction: 'close_menu',
                                width: mainLayoutStore.width,
                                show: !mainLayoutStore.showHelp
                            })
                        }}
                    />
                    {content}
                </div>
                <div
                    className={mainLayoutStore.showHelp ? "main_layout__rightbar rightbar_show" : "main_layout__rightbar rightbar_hide"}>
                    <div className="main_layout__rightbar_header">
                        <span className="rightbar_title">{rightbarTitle}</span>
                    </div>
                    <div className="main_layout__rightbar_content">
                        {rightbar}
                        <div className="main_layout__rightbar_footer">
                            <button className="rightbar_close" onClick={() => {
                                mainLayoutStore.toggleHelp();
                            }}>
                                Скрыть
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Main;