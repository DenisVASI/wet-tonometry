import React from 'react';
import './styles/App.sass';
import {createBrowserHistory} from "history";
import {Router, Switch, Route, Redirect} from "react-router";
import MainPage from "./pages/Main";
import PatientInfoPage from "./pages/PatientInfo";
import MassesPage from "./pages/Masses";
import UploadPage from "./pages/Upload";
import Store from "./mobx/Store";
import {observer, Provider} from "mobx-react";
import ResultPage from "./pages/Result";
import MethodeOfMaklakov from "./pages/MethodeOfMaklakov"
import MethodeOfElasto from "./pages/MethodeOfElasto"
import CalculationPage from "./pages/Сalculation"
import Login from "./pages/Login";
import Patients from "./pages/Patients";
import UploadStore from "./mobx/UploadStore";
import PatientStore from "./mobx/PatientStore";
import MainLayoutStore from "./mobx/MainLayoutStore";
import AuthStore from "./mobx/AuthStore";

const history = createBrowserHistory();

@observer
class App extends React.Component {

    constructor(props) {
        super(props);
        //stores
        this.store = new Store();
        this.uploadStore = new UploadStore();
        this.patientStore = new PatientStore();
        this.mainLayoutStore = new MainLayoutStore();
        this.authStore = new AuthStore();
        //end stores
        const authData = App.toJson(localStorage.getItem('key_m'));
        this.authStore.setAuthData(authData);
    }

    static toJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return null;
        }
        return JSON.parse(str);
    }

    render() {

        const {store, uploadStore, patientStore, mainLayoutStore, authStore} = this;

        return (
            <Provider
                store={store}
                uploadStore={uploadStore}
                patientStore={patientStore}
                mainLayoutStore={mainLayoutStore}
                authStore={authStore}
            >
                <div className="App">
                    <Router history={history}>
                        <Switch>
                            <Route path="/" component={MainPage} exact/>
                            <Route path="/login" component={Login} exact/>

                            <Route path="/upload" component={() => {
                                const allow = !!store.selectedMethod;
                                return allow ? <UploadPage/> : <Redirect to='/'/>;
                            }} exact/>
                            <Route path="/patient" component={() => {
                                const allow = store.selectedMethod && uploadStore.uploadResponse;
                                return allow ? <PatientInfoPage/> : <Redirect to='/upload'/>;
                            }} exact/>
                            <Route path="/masses" component={() => {
                                const allow = store.selectedMethod && uploadStore.uploadResponse && patientStore.patientForm;
                                return allow ? <MassesPage
                                    rows={uploadStore.uploadResponse && uploadStore.uploadResponse.eyes.length < 6 ? 6 : 4}
                                    imageFromCanvas={uploadStore.imageFromCanvas}
                                    uploadResponse={uploadStore.uploadResponse}
                                /> : <Redirect to='/patient'/>
                            }} exact/>

                            <Route path="/result" component={ResultPage} exact/>
                            <Route path="/methodemaklakov" component={MethodeOfMaklakov} exact/>
                            <Route path="/methodeelasto" component={MethodeOfElasto} exact/>
                            <Route path="/patients" component={Patients} exact/>
                            <Route path="/calculation" component={CalculationPage} exact/>
                        </Switch>
                    </Router>
                </div>
            </Provider>
        );
    }
}

export default App;
