import React from "react";
import {Link} from "react-router-dom";
import {Icon} from "antd";
import TnPopover from "./Popover";
import {inject, observer} from "mobx-react";

@inject('uploadStore', 'patientStore','store')
@observer
class Card extends React.Component {

    render() {

        const {image, store, uploadStore, patientStore} = this.props;

        return (
            <div className="card" style={{
                backgroundImage: `url(${image})`
            }}>
                <div className="card_overlay"/>
                <div className="card_content">
                    <div className="card_description">
                        Используйте этот модуль при проведении измерения внутриглазного давления
                        тонометрией по Маклакову. Этот метод предполагает использование
                        тонометров
                        массой 10 грамм.
                    </div>
                    <TnPopover content={
                        <div className="tn_popover__popover_body">
                            <div className="tn_popover__popover_item">
                                <Link to="/upload"
                                      onClick={() => {
                                          uploadStore.setFileBase64(null);
                                          uploadStore.setCroppedFile(null);
                                          uploadStore.setFile(null);
                                          uploadStore.setImageFromCanvas(null);
                                          store.setPatient(null);
                                          patientStore.setPatientForm(null);
                                          store.setMethod({
                                              key: 1,
                                              name: 'Маклаков'
                                          });
                                      }}>
                                    <button className="popover-btn">Первичный прием
                                    </button>
                                </Link>
                            </div>
                            <div className="tn_popover__popover_item">
                                <button className="popover-btn">
                                    Повторный прием
                                </button>
                            </div>
                        </div>
                    }>
                        <button className="card_btn">
                            Тонометрия по Маклакову <Icon
                            type="arrow-right"/>
                        </button>
                    </TnPopover>
                </div>
            </div>
        );
    }
}

export default Card;