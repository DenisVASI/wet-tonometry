import React from "react";
import {Icon} from "antd";

class TnAlert extends React.Component {

    state = {
        classes: 'tn_alert tn_alert_show'
    };

    render() {
        return (
            <div className={this.state.classes}>
                <div className="tn_alert__content">
                    <div className="tn_alert__icon">
                        <Icon type="alert"/>
                    </div>
                    <div className="tn_alert__text">
                        {this.props.text}
                    </div>
                </div>
                <div className="tn_alert__actions">
                    <button className="btn btn-blue" onClick={() => {
                        this.setState({classes: 'tn_alert tn_alert_hide'}, () => {
                            setTimeout(() => {
                                if (typeof this.props.help === "function") this.props.help();
                            }, 1000);
                        });
                    }}>Справка
                    </button>
                    <button className="btn btn-blue" onClick={() => {
                        this.setState({classes: 'tn_alert tn_alert_hide'}, () => {
                            setTimeout(() => {
                                if (typeof this.props.hide === "function") this.props.hide();
                            }, 1000);
                        });
                    }}>Закрыть
                    </button>
                </div>
            </div>
        );
    }
}

export default TnAlert;