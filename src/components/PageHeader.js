import React from "react";
import {Grid, Row, Col} from "react-flexbox-grid";
import {Dropdown, Icon, Menu} from "antd";

class TnPageHeader extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        const {children, menuAction, helpAction} = this.props;

        const menu = (
            <Menu>
                <Menu.Item>
                    Русский
                </Menu.Item>
                <Menu.Item>
                    English
                </Menu.Item>
                <Menu.Item>
                    Deutsch
                </Menu.Item>
            </Menu>
        );

        return (
            <div className="page_header">
                <div className="page_header_wrapper">
                    <Grid>
                        <Row>
                            <Col xs={4}>
                                <div className="page_header_items items_right">
                                    <div className="header_item" onClick={() => {
                                        if (typeof menuAction === "function") menuAction();
                                    }}>
                                        <Icon type="menu"/>
                                        <div>Меню</div>
                                    </div>
                                    <Dropdown overlay={menu}>
                                        <div className="header_item">
                                            <Icon type="global"/>
                                            <div>Русский</div>
                                        </div>
                                    </Dropdown>
                                </div>
                            </Col>
                            <Col xs={8}>
                                <Grid>
                                    <Row>
                                        <Col xs={8} md={10}>
                                            {children}
                                        </Col>
                                        <Col xs={4} md={2}>
                                            <div className="page_header_items items_left">
                                                <div className="header_item" onClick={() => {
                                                    if (typeof helpAction === "function") helpAction();
                                                }}>
                                                    <Icon type="question-circle"/>
                                                    <div>Справка</div>
                                                </div>
                                            </div>
                                        </Col>
                                    </Row>
                                </Grid>
                            </Col>
                        </Row>
                    </Grid>
                </div>
            </div>
        );
    }
}

export default TnPageHeader;