import React from "react";
import moment from "moment";

class TnPopover extends React.Component {

    constructor(props) {
        super(props);

        this.default = {
            hoverTimeout: 200,
            animationDelay: 500
        };

        this.state = {
            classes: "tn_popover",
            started: false,
            startedAt: null,
            finishedAt: null,
            loading: false
        };
    }

    render() {

        const {classes} = this.state;

        const {hoverTimeout: hoverTimeoutProps, animationDelay: animationDelayProps} = this.props;
        const {hoverTimeout: hoverTimeoutDefault, animationDelay: animationDelayDefault} = this.default;

        const hoverTimeout = hoverTimeoutProps ? hoverTimeoutProps : hoverTimeoutDefault;
        const animationDelay = animationDelayProps ? animationDelayProps : animationDelayDefault;

        return <div className={classes} onMouseLeave={() => {

            this.setState({started: false, finishedAt: moment().format('x')}, () => {

                let timeout = animationDelay + (this.state.startedAt - this.state.finishedAt);
                timeout = timeout < 0 ? 0 : timeout;

                if (classes !== "tn_popover") {
                    setTimeout(() => {
                        this.setState({classes: "tn_popover tn_popover_hide"}, () => {
                            setTimeout(() => {
                                this.setState({classes: "tn_popover", loading: false});
                            }, animationDelay);
                        });
                    }, timeout);
                }
            });

        }}>
            <div className="tn_popover__content_wrapper">
                <div className="tn_popover__content">
                    {this.props.content}
                </div>
            </div>
            <span className="tn_popover_button"
                onClick={() => {
                    if (!this.state.loading) this.setState({classes: "tn_popover tn_popover_show", loading: true});
                }} onMouseEnter={() => {

                if (!this.state.loading)
                    this.setState({started: true, startedAt: moment().format('x')}, () => {
                        setTimeout(() => {
                            const {started} = this.state;
                            if (started)
                                this.setState({classes: "tn_popover tn_popover_show", loading: true});
                        }, hoverTimeout);
                    });

            }}>{this.props.children}</span>
        </div>
    }
}

export default TnPopover;