class ImageProcessing {
    static Base64toBlob(base64) {
        let tmp = base64.split(',');
        let data = atob(tmp[1]);
        let mime = tmp[0].split(':')[1].split(';')[0];
        let buf = new Uint8Array(data.length);
        for (let i = 0; i < data.length; i++) {
            buf[i] = data.charCodeAt(i);
        }

        return new Blob([buf], {type: mime});
    }

    static blobToDataURL(blob, callback) {
        const a = new FileReader();
        a.onload = function (e) {
            callback(e.target.result);
        };
        a.readAsDataURL(blob);
    }
}

export default ImageProcessing;