import React from "react";
import {inject, observer} from "mobx-react";

@inject("store")
@observer
class Loader extends React.Component{
    render() {

        const classes = this.props.store.status ? 'loader loader-show' : 'loader';

        return (
            <div className={classes}>
                <div className="container-loader">
                    <div className="loader-box"></div>
                    <div className="loader-box"></div>
                    <div className="loader-box"></div>
                    <div className="loader-box"></div>
                    <div className="loader-box"></div>
                </div>
            </div>
        );
    }
}

export default Loader;