import React from "react";
import {Icon} from "antd";
import {Grid, Row} from "react-flexbox-grid";

class Breadcrump extends React.Component {

    render() {

        // const {showMenu, showHelp, width, toggleMenu, toggleHelp} = this.props.store;
        const {path} = this.props;

        return (
            <Grid>
                <Row>
                    <div className="page_header__wrapper">
                        <div className="page_breadcrumb">
                            {path && path.map(({name}, key) => {

                                if (key !== path.length - 1)

                                    return [<div key={name + key} className="page_breadcrumb__item">
                                        {name}
                                    </div>,
                                        <div key={key} className="page_breadcrumb__divider">
                                            <Icon type="arrow-right"/>
                                        </div>
                                    ];
                                else return <div key={name + key} className="page_breadcrumb__item item_blue">
                                    {name}
                                </div>
                            })}
                        </div>
                    </div>
                </Row>
            </Grid>
        );
    }
}

export default Breadcrump;