import React from "react";
import Main from "../../layout/Main";
import {inject, observer} from "mobx-react";
import {Form, Select} from "antd";
import {Redirect} from "react-router";
import {Col, Grid, Row} from "react-flexbox-grid";
import config from "../../config/production";
import PressureCalculation from "../../components/PressureСalculation";

const {Option} = Select;

@inject("store")
@observer
class MassesPage extends React.Component {

    state = {
        redirect: false,
        redirectTo: true,
        savedImage: null
    };

    static dataURLtoBlob(dataurl) {
        let arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new Blob([u8arr], {type: mime});
    }


    static average(nums) {
        if (nums && nums.length > 0)
            return nums.reduce((a, b) => (a + b)) / nums.length;
        else return -1;
    }

    handleSubmit = e => {
        e.preventDefault();
        const {store} = this.props;
        this.props.form.validateFields((err, values) => {
            if (!err) {
                //код для модуля сожедращего элестотонометрию
                const p5 = PressureCalculation.get_p5({
                    values: values,
                    rations: store.uploadResponse.ratios
                });

                const p10 = PressureCalculation.get_p10({
                    values: values,
                    rations: store.uploadResponse.ratios
                });

                const p15 = PressureCalculation.get_p15({
                    values: values,
                    rations: store.uploadResponse.ratios
                });

                if (
                    (p5.left_eye.length && p5.right_eye.length) &&
                    (p10.left_eye.length && p10.right_eye.length) &&
                    (p15.left_eye.length && p15.right_eye.length)
                ) {


                    const {
                        p5_0_l,
                        p5_0_r,
                        p10_0_l,
                        p10_0_r,
                        p15_0_l,
                        p15_0_r
                    } = PressureCalculation.get_p0_for_different_mass(p5, p10, p15);


                    const {ep_l, ep_r} = PressureCalculation.get_elastoplasticity(p15_0_l, p5_0_l, p15_0_r, p5_0_r);


                    const {p0_r, p0_l} = PressureCalculation.get_approximate_value(p5, p10, p15);

                    console.log(p5_0_l, p5_0_r, p10_0_l, p10_0_r, p15_0_l, p15_0_r);

                    store.setMode("e", {
                        p0_l,
                        p0_r,
                        ep_l,
                        ep_r,
                        p5_0_l,
                        p5_0_r,
                        p10_0_l,
                        p10_0_r,
                        p15_0_l,
                        p15_0_r
                    });

                    console.log(store.elastoData);
                } else store.setMode("m");
                //код для модуля сожедращего элестотонометрию


                const json = {
                    ratios: store.uploadResponse.ratios,
                    masses: Object.keys(values).map(item => {
                        const mass = values[item].split('-')[0];
                        return values[item] !== "n" ? mass : 0
                    }),
                    eyes: Object.keys(values).map(item => {
                        return values[item].split('-')[1];
                    }),
                };

                //сохраняем массы чтобы отправить в историю
                store.setMasses(json.masses);

                const pressures = json.ratios.map(r => {
                    return 931.65 * Math.pow((config.ration * r), -2.048);
                });

                //сохраняем pressures чтобы отправить в историю
                store.setPressures(pressures);

                let left = [];
                let right = [];

                for (let i = 0; i < pressures.length; i++) {
                    if (json.eyes[i] === "0") left.push(pressures[i]);
                    else if (json.eyes[i] === "1") right.push(pressures[i]);
                }

                const av_left = MassesPage.average(left);
                const av_right = MassesPage.average(right);

                let req_data = {
                    Pt: av_left > av_right ? av_left : av_left,
                    Stage: parseInt(store.preparationsForm.stage),
                    PG: store.preparationsForm.medication.indexOf('Простагландины') + 1 ? 1 : 0,
                    AM: store.preparationsForm.medication.indexOf('Адреномиметики') + 1 ? 1 : 0,
                    IKA: store.preparationsForm.medication.indexOf('Ингибиторы карбоангидразы') + 1 ? 1 : 0,
                    BB: store.preparationsForm.medication.indexOf('Бета блокаторы') + 1 ? 1 : 0,
                    Retinaprotection: store.preparationsForm.medication.indexOf('Ретинопротекция') + 1 ? 1 : 0
                };

                req_data.Sum = req_data.AM + req_data.BB + req_data.IKA + req_data.PG;

                const blob = new Blob([JSON.stringify(req_data, null, 2)], {type: 'application/json'});

                let FD = new FormData();
                FD.append("json", blob);

                store.setAVLeft(av_left);
                store.setAVRight(av_right);
                store.getRecomendation(FD, () => {
                    this.setState({redirect: true, redirectTo: '/result'});
                });

            }
        });
    };

    render() {

        if (this.state.redirect && this.state.redirectTo) return <Redirect to={this.state.redirectTo}/>;

        const {getFieldDecorator} = this.props.form;

        const {imageFromCanvas, rows, uploadResponse} = this.props;

        return (
            <Main
                stage={4}
                content={
                    <div className="recomendations_page">
                        <div className="recomendations_page__form">
                            {imageFromCanvas && <img style={{
                                width: "100%",
                                borderRadius: '15px'
                            }} src={imageFromCanvas} alt="cropped image"/>}
                            <p style={{
                                marginTop: '20px'
                            }}>Выберите оттиск и вес грузика:</p>
                            <Form onSubmit={this.handleSubmit}>
                                <Grid>
                                    <Row>
                                        {uploadResponse && uploadResponse.eyes.map((item, key) => {
                                            return <Col xs={6} md={rows} key={key}>
                                                <Form.Item label={`Оттиск ${key + 1}`}>
                                                    {getFieldDecorator(`eye${key}`, {
                                                        rules: [{required: true, message: 'Укажите оттиск'}],
                                                        initialValue: `${uploadResponse.masses[key].toString()}-${item.toString()}`
                                                    })(
                                                        <Select
                                                            placeholder="Укажите оттиск"
                                                        >
                                                            <Option value="5-0">5г - Левый оттиск</Option>
                                                            <Option value="5-1">5г - Правый оттиск</Option>
                                                            <Option value="10-0">10г - Левый оттиск</Option>
                                                            <Option value="10-1">10г - Правый оттиск</Option>
                                                            <Option value="15-0">15г - Левый оттиск</Option>
                                                            <Option value="15-1">15г - Правый оттиск</Option>
                                                            <Option value="n">Не тонометрический круг</Option>
                                                        </Select>,
                                                    )}
                                                </Form.Item>
                                            </Col>
                                        })}
                                    </Row>
                                </Grid>

                                <Grid>
                                    <button className="btn btn-blue">Отправить</button>
                                </Grid>

                            </Form>
                        </div>
                    </div>

                }
                rightbarTitle="Массы: справка"
                rightbar={
                    <div>
                        <div className="help">
                            <div className="help__card">
                                Проверьте корректность масс которые определил наш сервис
                            </div>
                        </div>
                    </div>
                }
            />
        );
    }
}

export default Form.create({name: 'masses_form'})(MassesPage);