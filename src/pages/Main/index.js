import React from "react";
import Main from "../../layout/Main";
import {Grid, Col, Row} from "react-flexbox-grid";
import {Redirect} from "react-router-dom";
import {inject, observer} from "mobx-react";
import Card from "../../components/Card";
import TnPageHeader from "../../components/PageHeader";

@inject("uploadStore", "store")
@observer
class MainPage extends React.Component {

    state = {
        redirect: false,
        redirectTo: null
    };

    render() {


        const {redirect, redirectTo} = this.state;

        if (redirect && redirectTo) {
            return <Redirect to={redirectTo}/>
        }

        return (
            <Main
                stage={1}
                content={
                    <div className="main_page">
                        <div className="main_page__categories">
                            <Grid>
                                <Row>
                                    <Col xs={12} md={6}>
                                        <Card image={require("../../images/testPage/mak3.jpeg")}/>
                                    </Col>
                                    {/*<Col xs={12} md={6}>*/}
                                    {/*<Card image={require("../../images/testPage/nemak.jpeg")}/>*/}
                                    {/*</Col>*/}
                                </Row>
                            </Grid>
                        </div>
                    </div>
                }
                rightbarTitle="Выбор категории: справка"
                rightbar={
                    <div>
                        <div className="help">
                            <div className="help__card">
                                Мы представляем Вам автоматизированный инструмент разметки тонограмм. Для начала работы
                                выберите тип процедуры.
                            </div>
                        </div>
                    </div>
                }
            />
        );
    }
}

export default MainPage;