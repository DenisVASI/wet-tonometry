import React from 'react';
import Main from '../../layout/Main'
import { Typography, Divider, Button } from 'antd';
const { Title, Paragraph, Text } = Typography;

class MethodeOfElasto extends React.Component {
    render (){
        return (<Main
        content = {<div>
            <Typography style = {{marginBottom:'20px'}}>
                <Title>Методика Эластотонометрии</Title>
                <Paragraph>
                    Эластотонометрия является ценным методом ранней диагностики и мониторинга глаукомы,
                    при этом в качестве диагностических критериев используется величина эластоподъема
                    (т.е. разницу между показаниями 15- и 5граммовых тонометров) и излом (изгиб) эластокривой.
                    Также этот метод диагностики позволяет получить значение истинного внутриглазного давления (P0).
                </Paragraph>
            </Typography>
            <a href="https://drive.google.com/file/d/1QH7BdHG7pTzTn8s1mLG5_2wcWWIQoOaj/view?usp=sharing">
                <Button type="primary" icon="download" size={'large'} > Скачать полный текст </Button>
            </a>

        </div>

        }


        />)
    }
}
export default MethodeOfElasto;
