import React from "react";
import Main from "../../layout/Main";
import {inject, observer} from "mobx-react";
import {Redirect} from "react-router";
import axios from "axios";
import config from "../../config/production";
import {Button, Table, Tag} from "antd";
import TnPageHeader from "../../components/PageHeader";

const ButtonGroup = Button.Group;

@inject("store")
@observer
class Patients extends React.Component {

    state = {
        redirect: false,
        redirectTo: null,
        patients: null
    };

    async componentDidMount() {

        if (!this.props.store.authData)
            this.setState({redirect: true, redirectTo: '/'});

        else {

            const {authData} = this.props.store;

            const response = await axios({
                method: 'POST',
                url: `${config.node_protocol}${config.node_url}:${config.node_port}/api/v1/patients/list`,
                headers: {
                    Authorization: `Token ${authData.user.token}`
                }
            }).then(res => res.data).catch(e => {
                console.log(e);
                return null;
            });

            if (response) {
                this.setState({patients: response.patients});
            }
        }
    }

    render() {

        const {redirect, redirectTo, patients} = this.state;

        if (redirect && redirectTo) {
            return <Redirect to={redirectTo}/>
        }

        const columns = [
            {
                title: 'ID',
                dataIndex: 'pat_id',
                key: 'patient_id',
            },
            {
                title: 'E-Mail',
                dataIndex: 'mail',
                key: 'patient_email',
            },
            {
                title: 'Метки',
                dataIndex: 'label',
                key: 'patient_tags',
                render: label => (<span>
                    {label.labels.map((lb, key) => {
                        return (
                            <Tag color="geekblue" key={key}>
                                {lb.toUpperCase()}
                            </Tag>
                        );
                    })}
                </span>)
            },
            {
                title: 'Действия',
                dataIndex: 'pat_id',
                key: 'patient_actions',
                render: id => {
                    return (
                        <ButtonGroup>
                            <Button type="primary" size="small">
                                История лечения
                            </Button>
                            <Button type="primary" size="small" onClick={() => {
                                this.props.store.setPatient(id, () => {
                                    this.props.store.setMethod({
                                        key: 1,
                                        name: 'Маклаков'
                                    });
                                    this.setState({
                                        redirect: true,
                                        redirectTo: '/upload'
                                    });
                                });
                            }}>
                                Провести измерения
                            </Button>
                        </ButtonGroup>
                    );
                }
            },
        ];

        return (
            <Main
                content={
                    <div className="patients_page">
                        <TnPageHeader/>
                        <div className="patients_page__table">
                            {patients &&
                            <Table dataSource={patients} columns={columns} rowKey={(_, key) => key}/>}
                        </div>
                    </div>
                }
            />
        );
    }
}

export default Patients;