import React from 'react';
import Main from '../../layout/Main'
import {Grid, Col, Row} from "react-flexbox-grid";
import {Form, Icon, Input, Button} from 'antd';
import Breadcrump from "../../components/Breadcrump";
import {inject, observer} from "mobx-react";

function hasErrors(fieldsError) {
    return Object.keys(fieldsError).some(field => fieldsError[field]);
}

@inject("store")
@observer
class CalculationPage extends React.Component {
    componentDidMount() {
        // To disabled submit button at the beginning.
        this.props.form.validateFields();
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
                this.props.store.SetCalculationValues(values);
            }
        });
    };

    render() {
        const {getFieldDecorator, getFieldsError} = this.props.form;

        return (
            <Main content={
                <div>
                    <Breadcrump path={[
                        {
                            name: '/'
                        },
                        {
                            name: 'Калькулятор'
                        }
                    ]}/>
                    <Form layout="inline" onSubmit={this.handleSubmit}>
                        <Grid>
                            <Row>
                                <Col xs={12} md={3}>
                                    <h3>Pcc OD</h3>
                                    <p>Роговично-компенсированное ВГД</p>
                                    <Form.Item>
                                        {getFieldDecorator('PccOD', {})(
                                            <Input
                                                placeholder="Pcc OD"
                                            />,
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col xs={12} md={3}>
                                    <h3>Pg OS</h3>
                                    <p>ВГД по Гольдману</p>
                                    <Form.Item>
                                        {getFieldDecorator('PgOS', {})(
                                            <Input
                                                placeholder="Pg OS"
                                            />,
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col xs={12} md={3}>
                                    <h3>PccOD</h3>
                                    <p>Роговично-компенсированное ВГД</p>
                                    <Form.Item>
                                        {getFieldDecorator('PccOD', {})(
                                            <Input
                                                placeholder="Pcc OD"
                                            />,
                                        )}
                                    </Form.Item>
                                </Col>
                                <Col xs={12} md={3}>
                                    <h3>PgOS</h3>
                                    <p>ВГД по Гольдману</p>
                                    <Form.Item>
                                        {getFieldDecorator('PgOS', {})(
                                            <Input
                                                placeholder="Pg OS"
                                            />,
                                        )}
                                    </Form.Item>
                                </Col>
                            </Row>
                        </Grid>
                        <Form.Item>
                            <Button type="primary" htmlType="submit" disabled={hasErrors(getFieldsError())}>
                                Расчитать
                            </Button>
                        </Form.Item>
                    </Form>


                    <Grid>
                        <Row>
                            <Col xs={12} md={4}>
                                PgOD: {this.props.store.calc && this.props.store.calc[0]}
                                PccOD: {this.props.store.calc && this.props.store.calc[2]}
                                CHOD: {this.props.store.calc && this.props.store.calc[4]}
                                CRFOD: {this.props.store.calc && this.props.store.calc[6]}
                                KbsOD: {this.props.store.calc && this.props.store.calc[8]}
                            </Col>
                            <Col xs={12} md={4}>
                                PgOS: {this.props.store.calc && this.props.store.calc[1]}
                                PccOS: {this.props.store.calc && this.props.store.calc[3]}
                                CHOS: {this.props.store.calc && this.props.store.calc[5]}
                                CRFOS: {this.props.store.calc && this.props.store.calc[7]}
                                KbsOS: {this.props.store.calc && this.props.store.calc[9]}
                            </Col>
                        </Row>
                    </Grid>

                </div>
            }/>
        );
    }
}

export default Form.create({name: 'calculation_form'})(CalculationPage);