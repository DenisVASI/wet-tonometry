import React from "react";
import {Checkbox, Form, Icon, Input, Button} from "antd";
import axios from "axios";
import config from "../../config/production";
import {inject, observer} from "mobx-react";
import {Redirect} from "react-router";

@inject("store")
@observer
class Login extends React.Component {

    state = {
        redirect: false,
        redirectTo: null
    };

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields(async (err, values) => {
            if (!err) {
                const response = await axios({
                    method: 'post',
                    url: `${config.node_protocol}${config.node_url}:${config.node_port}/api/v1/auth/login`,
                    data: {
                        user: {
                            email: values.email,
                            password: values.password
                        }
                    }
                }).then(res => res.data).catch(e => {
                    console.log(e);
                    return null;
                });

                if (response) {
                    this.props.store.setAuthData(response, () => {
                        localStorage.setItem('key_m', JSON.stringify(response));
                    });
                } else {
                    this.props.store.setAuthData(null);
                }
            }
        });
    };

    componentDidMount() {
        if (this.props.store.authData) {
            this.setState({
                redirect: true,
                redirectTo: '/'
            });
        }
    }

    render() {

        const {redirect, redirectTo} = this.state;

        if (redirect && redirectTo) {
            return <Redirect to={redirectTo}/>
        }

        const {getFieldDecorator} = this.props.form;

        return (
            <div className="login_page">
                <Form onSubmit={this.handleSubmit.bind(this)} className="login-form">
                    <Form.Item>
                        {getFieldDecorator('email', {
                            rules: [{required: true, message: 'Пожалуйста введите ваш e-mail!'}],
                        })(
                            <Input
                                prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                placeholder="Электронная почта"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator('password', {
                            rules: [{required: true, message: 'Пожалуйста введите ваш пароль!'}],
                        })(
                            <Input
                                prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                type="password"
                                placeholder="Пароль"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator('remember', {
                            valuePropName: 'checked',
                            initialValue: true,
                        })(<Checkbox>Запомнить меня</Checkbox>)}

                        <Button type="primary" htmlType="submit" className="login-form-button">
                            Войти
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        );
    }
}


export default Form.create({name: "login_form"})(Login);