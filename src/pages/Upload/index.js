import React from "react";
import Main from "../../layout/Main";
import {Grid, Row, Col} from "react-flexbox-grid";
import {inject, observer} from "mobx-react";
import Cropper from 'react-cropper';
import 'cropperjs/dist/cropper.css';
import {Redirect} from "react-router";
import {Icon} from 'antd';
import ImageProcessing from "../../components/ImageProcessing";
import {Upload, message} from 'antd';

const {Dragger} = Upload;

@inject("uploadStore", "mainLayoutStore")
@observer
class UploadPage extends React.Component {

    state = {
        redirect: false,
        redirectTo: null,
        rotate: 0,
        imageUrl: null
    };

    constructor(props) {
        super(props);
        this.cropper = React.createRef();
    }

    _crop() {
        const {uploadStore} = this.props;
        uploadStore.setCroppedFile(
            this.cropper.current.getCroppedCanvas().toDataURL('image/jpeg')
        );

        uploadStore.sendCroppedFileToApi(
            ImageProcessing.Base64toBlob(uploadStore.croppedFile),
            () => {
                if (uploadStore.processedImage) {
                    let canvas = document.createElement('canvas');
                    let context = canvas.getContext('2d');
                    let imageObj = new Image();
                    imageObj.crossOrigin = "Anonymous";
                    imageObj.onload = () => {
                        // draw cropped image
                        let sourceX = uploadStore.processedImage.coordinates.x;
                        let sourceY = uploadStore.processedImage.coordinates.y;
                        let sourceWidth = uploadStore.processedImage.sizes.w;
                        let sourceHeight = uploadStore.processedImage.sizes.h;
                        let destWidth = sourceWidth;
                        let destHeight = sourceHeight;
                        let destX = 0;
                        let destY = 0;
                        canvas.width = sourceWidth;
                        canvas.height = sourceHeight;
                        context.drawImage(imageObj, sourceX, sourceY, sourceWidth, sourceHeight, destX, destY, destWidth, destHeight);
                        const base64 = canvas.toDataURL("image/jpeg");
                        console.log('base64 from canvas', base64);
                        uploadStore.setImageFromCanvas(base64);
                    };
                    imageObj.src = uploadStore.savedImage;
                }
                this.setState({redirect: true, redirectTo: '/patient'});
            },
            () => {
                message.error('Фотография не содержит оттистки или имеет плохое качество!');
                uploadStore.setFileBase64(null);
            }
        );
    }

    rotateLeft() {
        this.cropper.current.rotate(90);
    }

    rotateRight() {
        this.cropper.current.rotate(-90);
    }

    beforeUpload(file) {
        const {uploadStore} = this.props;
        const isJpgOrPng = file.type.toLocaleLowerCase() === 'image/jpeg' || file.type.toLocaleLowerCase() === 'image/png';
        if (!isJpgOrPng) {
            message.error('Поддерживаются только фотографии JPG/PNG формата!');
        } else {
            uploadStore.setFile(file);
            uploadStore.setCroppedFile(null);
            ImageProcessing.blobToDataURL(file, url => {
                uploadStore.setFileBase64(url);
            });
        }

        //чтобы дропзона не делала автоматическую отправку нужно всегда отдавать false
        return false;
    }

    render() {

        const {uploadStore, mainLayoutStore} = this.props;

        // console.log(mainLayoutStore.width);

        if (this.state.redirect) return <Redirect to={this.state.redirectTo}/>;

        const uploadProps = {
            name: 'file',
            multiple: false,
            beforeUpload: this.beforeUpload.bind(this),
            showUploadList: false
        };

        return (
            <Main
                stage={2}
                content={
                    <div className="upload_page">
                        <div className="upload_page__from">
                            <div className="upload_page__upload">
                                <Grid>
                                    <Row>
                                        <Col xs={12}>
                                            {!uploadStore.base64file && <div className="instruments">
                                                <Dragger {...uploadProps}>
                                                    <p className="ant-upload-drag-icon">
                                                        <Icon type="plus"/>
                                                    </p>
                                                    <p className="ant-upload-text">Нажмите на + для загрузки фотографии
                                                        или
                                                        перетяните ее из папки</p>
                                                    <p className="ant-upload-hint">
                                                        Поддерживаются фотографии JPEG/JPG и PNG формата
                                                    </p>
                                                </Dragger>
                                            </div>}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={12}>
                                            {!uploadStore.croppedFile && uploadStore.base64file &&
                                            <div className="upload_page__cropper">

                                                {mainLayoutStore.width > 420 && <Cropper
                                                    ref={this.cropper}
                                                    cropBoxMovable={true}
                                                    cropBoxResizable={true}
                                                    scalable={false}
                                                    dragMode="move"
                                                    src={uploadStore.base64file}
                                                    style={{height: '400px', width: '100%'}}
                                                    initialAspectRatio={16 / 9}
                                                    guides={true}
                                                />}

                                                {!(mainLayoutStore.width > 420) && <Cropper
                                                    ref={this.cropper}
                                                    cropBoxMovable={false}
                                                    cropBoxResizable={false}
                                                    scalable={false}
                                                    dragMode="move"
                                                    src={uploadStore.base64file}
                                                    style={{height: '400px', width: '100%'}}
                                                    initialAspectRatio={16 / 9}
                                                    guides={true}
                                                />}

                                                <div className="upload_page__cropper_actions panel">
                                                    <Grid>
                                                        <Row>
                                                            {mainLayoutStore.width > 420 ? <Col xs={4}>
                                                                <div className="panel_items items_left">
                                                                    <div className="panel_item" onClick={() => {
                                                                        this.rotateLeft();
                                                                    }}>
                                                                        <Icon type="reload" style={{
                                                                            transform: 'scale(-1, 1)'
                                                                        }}/>
                                                                        <div>Влево</div>
                                                                    </div>
                                                                    <div className="panel_item" onClick={() => {
                                                                        this.rotateRight();
                                                                    }}>
                                                                        <Icon type="reload"/>
                                                                        <div>Вправо</div>
                                                                    </div>
                                                                    <div className="panel_item" onClick={() => {
                                                                        this._crop();
                                                                    }}>
                                                                        <Icon type="save"/>
                                                                        <div>Сохранить</div>
                                                                    </div>
                                                                </div>

                                                            </Col> : [
                                                                <Col xs={4} key={'lf'}>
                                                                    <div className="panel_item" onClick={() => {
                                                                        this.rotateLeft();
                                                                    }}>
                                                                        <Icon type="reload" style={{
                                                                            transform: 'scale(-1, 1)'
                                                                        }}/>
                                                                        <div>Влево</div>
                                                                    </div>
                                                                </Col>,
                                                                <Col xs={4} key={'rt'}>
                                                                    <div className="panel_item" onClick={() => {
                                                                        this.rotateRight();
                                                                    }}>
                                                                        <Icon type="reload"/>
                                                                        <div>Вправо</div>
                                                                    </div>
                                                                </Col>,
                                                                <Col xs={4} key={'sv'}>
                                                                    <div className="panel_item" onClick={() => {
                                                                        this._crop();
                                                                    }}>
                                                                        <Icon type="save"/>
                                                                        <div>Сохранить</div>
                                                                    </div>
                                                                </Col>
                                                            ]
                                                            }
                                                        </Row>
                                                    </Grid>
                                                </div>
                                            </div>}
                                            {uploadStore.imageFromCanvas &&
                                            <div className="upload_page__cropper_result">
                                                <div className="upload_page__result_instruments">
                                                    <button>Загрузить новое фото</button>
                                                    <button>Ввести информацию о пациенте</button>
                                                </div>
                                                <img src={uploadStore.imageFromCanvas} alt="cropped image"/>
                                            </div>}
                                        </Col>
                                    </Row>
                                </Grid>
                            </div>
                        </div>
                    </div>

                }
                rightbarTitle="Загрузка фото: справка"
                rightbar={
                    <div>
                        <div className="help">
                            <div className="help__card">
                                Загрузите фотографию, для этого нажмите на соответсвующую кнопку и выберите фотографию
                                из памяти телефона или сделайте новую. После этого появится форма для редактирования.
                            </div>
                            <div className="help__card">
                                Переверните
                                фотографию так, чтобы оттиски слева соответствовали правому глазу, а оттиски справа -
                                левому.
                                Обрежте фотографию так, чтобы часть с оттисками занимала большую часть фотографии.
                            </div>
                            <div className="help__card">
                                После этого нажмите на кнопку "Подтвердить".
                            </div>
                        </div>
                    </div>
                }/>
        );
    }
}

export default UploadPage;