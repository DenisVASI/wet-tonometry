import React from "react"
import Main from "../../layout/Main";
import {inject, observer} from "mobx-react";
import {Redirect} from "react-router";
import {Modal, Button, Form, Input} from 'antd';
import axios from "axios";
import config from "../../config/production";
import TnPageHeader from "../../components/PageHeader";
import { Card, Icon, Avatar } from 'antd';
import {Grid, Row, Col} from "react-flexbox-grid";

const { Meta } = Card;
const {TextArea} = Input;

@inject("store")
@observer
class ResultPage extends React.Component {

    state = {
        message: '',
        visible: false,
        toSave: null
    };


    handleOk = () => {
        this.props.store.setStatus(true);
        setTimeout(() => {
            this.props.store.setStatus(false);
            this.setState({visible: false});
        }, 3000);
    };

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    };

    handleCancel = () => {
        this.setState({visible: false});
    };

    showModal = () => {
        this.setState({
            visible: true,
        });
    };

    componentDidMount() {

        if (!this.props.store.uploadResponse) {
            this.setState({redirect: true, redirectTo: '/upload'});
        } else if (!this.props.store.selectedMethod) {
            this.setState({redirect: true, redirectTo: '/'});
        } else if (!this.props.store.preparationsForm) {
            this.setState({redirect: true, redirectTo: '/preparations'});
        } else if (!this.props.store.result) {
            this.setState({redirect: true, redirectTo: '/masses'});
        }

        const res = this.props.store.result;
        let message = "";
        if (res) {
            if (res.rec_drug === "drug_PG") message += "Рекомендуем перейти с текущего лечения на простагландины.";
            if (res.rec_drug === "drug_PG_BB") message += "Рекомендуем перейти с текущего лечения на комбенацию простагландины + бета блокаторы.";
            if (res.add_drug === "drug_PG") message += "Рекомендуем добавить к текущему лечению простагландины.";
            if (res.add_drug === "drug_PG_BB") message += "Рекомендуем добавить к текущему лечению комбинацию простагландины + бета блокаторы.";
            if (res.add_drug === "drug_PG_BB, drug_AM") message += "Рекомендуем добавить к текущему лечению комбинацию простагландины + бета блокаторы и адреномиметики.";
            if (res.add_drug === "drug_AM") message += "Рекомендуем добавить к текущему лечению комбинацию адреномиметики.";
            if (res.add_drug === "drug_IKA") message += "Рекомендуем добавить к текущему лечению ингибиторы карбоангидразы.";
            if (res.RP === "1") message += "Необходимо назначить ретинопротекцию.";
            if (res.time === "3 month") message += "Назначить повторный осмотр через 3 месяца.";
            if (res.time === "2 week") message += "Назначить повторный осмотр через 2 недели.";
            if (res.diagnostic === "VGD") message += "Необходим контроль внутриглазного давления.";
            if (res.diagnostic === "operation") message += "Необходимо готовить пациента к оперативному лечению.";
            if (res.diagnostic === "field and OKT") message += "Необходим контроль полей зрения и проведение оптической когерентной томографии.";
            if (
                res.time === 0 &&
                res.rec_drug === 0 &&
                res.diagnostic === 0 &&
                res.add_drug === 0 &&
                res.RP === 0
            ) {
                message += "Пациент здоров";
            }
        }

        this.setState({
            message: message
        }, async () => {

            const {store} = this.props;

            if (this.props.store.patientForMeasurement !== null) {
                const requestData = {
                    image: store.savedImage,
                    patientId: store.patientForMeasurement,
                    coordinates: {
                        coordinates: store.processedImage.coordinates,
                        sizes: store.processedImage.sizes
                    },
                    masses: {data: store.masses},
                    rations: {data: store.uploadResponse.ratios},
                    drug_type: {data: store.preparationsForm.medication},
                    pressureSource: {data: store.pressures}
                };

                this.setState({toSave: requestData});



            }
        });
    }

    render() {

        if (this.state.redirect && this.state.redirectTo)
            return <Redirect to={this.state.redirectTo}/>;

        const {getFieldDecorator} = this.props.form;

        const {store} = this.props;
        const {message, toSave} = this.state;

        return (
            <Main
                content={
                    <div className="result_page">
                        {store.mode === "m" ? <div className="result_page__form">
                            <div>
                                <h2>Результат измeерений: </h2>
                                {(store.avR && store.avR !== -1) &&
                                <p>ВГД по Маклакову для правого глаза: {store.avR.toFixed(2)} мм.рт.ст</p>}
                                {(store.avL && store.avL !== -1) &&
                                <p>ВГД по Маклакову для левого глаза: {store.avL.toFixed(2)} мм.рт.ст</p>}
                                <h2>Рекомендации:</h2>
                                {message && <p>
                                    {message}
                                </p>}

                                {toSave && <button className="btn btn-blue" onClick={async () => {
                                    await axios({
                                        method: 'POST',
                                        url: `${config.node_protocol}${config.node_url}:${config.node_port}/api/v1/history/save`,
                                        data: {
                                            diagnosticsData: toSave
                                        },
                                        headers: {
                                            Authorization: `Token ${store.authData.user.token}`
                                        }
                                    }).catch(e => {
                                        console.log(e);
                                    });
                                }}>Сохранить в историю лечения
                                </button>}

                            </div>
                            {/*{JSON.stringify(this.props.store.result)}*/}

                            <div>
                                <h2>Обратная связь: </h2>

                                <p>Вы в числе первых, кто опробовал наш сервис. Пожалуйста, дайте нам обратную связь,
                                    расскажите о ваших впечатлениях или идеях и пожеланиях.
                                    Оставьте любые контактные данные и мы расскажем вам, когда ждать обновлений и сможем
                                    ли мы внедрить предложенные вами решения.</p>

                                <div style={{
                                    marginTop: 20
                                }}>
                                    Вы можете связаться с нами:
                                    <div style={{
                                        marginTop: 10,
                                        marginBottom: 10
                                    }}>
                                        <a href="mailto:Sany_re@list.ru">sany_re@list.ru - Александр Горобец, head of
                                            development</a>
                                    </div>
                                    <div style={{
                                        marginBottom: 10
                                    }}>
                                        <a href="mailto:denisvasilenkoise@outlook.com">denisvasilenkoise@outlook.com -
                                            Василенко Денис, программист</a>
                                    </div>
                                </div>
                            </div>

                            {/*<button style={{*/}
                            {/*marginTop: 10*/}
                            {/*}} className="btn btn-blue" onClick={this.showModal}>Оставьте ваш отзыв*/}
                            {/*</button>*/}

                            <Modal
                                visible={this.state.visible}
                                title="Обратная связь"
                                onOk={this.handleOk}
                                onCancel={this.handleCancel}
                                footer={[
                                    <Button key="back" onClick={this.handleCancel}>
                                        Закрыть
                                    </Button>,
                                    <Button key="submit" type="primary" onClick={this.handleSubmit}>
                                        Отправить
                                    </Button>,
                                ]}
                            >
                                <Form onSubmit={this.handleSubmit} className="feedback-form">
                                    <Form.Item label="Любые ваши контактные данные(по желанию)">
                                        {getFieldDecorator('userdata', {
                                            rules: [{required: false}],
                                        })(
                                            <Input
                                                prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                                placeholder="Username"
                                            />,
                                        )}
                                    </Form.Item>
                                    <Form.Item label="Текст отзыва">
                                        {getFieldDecorator('review', {
                                            rules: [{required: true, message: 'Пожалуйста, напишите ваш отзыв'}],
                                        })(
                                            <TextArea

                                            />,
                                        )}
                                    </Form.Item>
                                </Form>
                            </Modal>
                        </div> : <div className="result_page__form">
                            <Grid>
                                <Row>
                                    <Col xs={12} md={6}>
                                        <Card
                                            style={{ width: '100%' }}
                                        >
                                            <Meta
                                                avatar={<div className="tn-icon">p0</div>}
                                                title={store.elastoData.p0_l.toFixed(2)}
                                                description="p0 для левого глаза"
                                            />
                                        </Card>
                                    </Col>
                                    <Col xs={12} md={6}>
                                        <Card
                                            style={{ width: '100%' }}
                                        >
                                            <Meta
                                                avatar={<div className="tn-icon">p0</div>}
                                                title={store.elastoData.p0_r.toFixed(2)}
                                                description="p0 для правого глаза"
                                            />
                                        </Card>
                                    </Col>
                                    <Col xs={12} md={6}>
                                        <Card
                                            style={{ width: '100%' }}
                                        >
                                            <Meta
                                                avatar={<div className="tn-icon">ЭП</div>}
                                                title={store.elastoData.ep_l.toFixed(2)}
                                                description="Эластоподъем для левого глаза"
                                            />
                                        </Card>
                                    </Col>
                                    <Col xs={12} md={6}>
                                        <Card
                                            style={{ width: '100%' }}
                                        >
                                            <Meta
                                                avatar={<div className="tn-icon">ЭП</div>}
                                                title={store.elastoData.ep_r.toFixed(2)}
                                                description="Эластоподъем для правого глаза"
                                            />
                                        </Card>
                                    </Col>
                                    <Col xs={12} md={6}>
                                        <Card
                                            style={{ width: '100%' }}
                                        >
                                            <Meta
                                                avatar={<div className="tn-icon">5Л</div>}
                                                title={store.elastoData.p5_0_l.toFixed(2)}
                                                description="Давление для левого галаза с 5-ти грамовым тонометром"
                                            />
                                        </Card>
                                    </Col>
                                    <Col xs={12} md={6}>
                                        <Card
                                            style={{ width: '100%' }}
                                        >
                                            <Meta
                                                avatar={<div className="tn-icon">5П</div>}
                                                title={store.elastoData.p5_0_r.toFixed(2)}
                                                description="Давление для правого галаза с 5-ти грамовым тонометром"
                                            />
                                        </Card>
                                    </Col>
                                    <Col xs={12} md={6}>
                                        <Card
                                            style={{ width: '100%' }}
                                        >
                                            <Meta
                                                avatar={<div className="tn-icon">10Л</div>}
                                                title={store.elastoData.p10_0_l.toFixed(2)}
                                                description="Давление для левого галаза с 10-ти грамовым тонометром"
                                            />
                                        </Card>
                                    </Col>
                                    <Col xs={12} md={6}>
                                        <Card
                                            style={{ width: '100%' }}
                                        >
                                            <Meta
                                                avatar={<div className="tn-icon">10П</div>}
                                                title={store.elastoData.p10_0_r.toFixed(2)}
                                                description="Давление для правого галаза с 10-ти грамовым тонометром"
                                            />
                                        </Card>
                                    </Col>
                                    <Col xs={12} md={6}>

                                        <Card
                                            style={{ width: '100%' }}
                                        >
                                            <Meta
                                                avatar={<div className="tn-icon">15Л</div>}
                                                title={store.elastoData.p15_0_l.toFixed(2)}
                                                description="Давление для левого галаза с 15-ти грамовым тонометрои"
                                            />
                                        </Card>
                                    </Col>
                                    <Col xs={12} md={6}>
                                        <Card
                                            style={{ width: '100%' }}
                                        >
                                            <Meta
                                                avatar={<div className="tn-icon">15П</div>}
                                                title={store.elastoData.p15_0_r.toFixed(2)}
                                                description="Давление для правого галаза с 15-ти грамовым тонометрои"
                                            />
                                        </Card>
                                    </Col>
                                </Row>
                            </Grid>
                        </div>}

                    </div>
                }
                rightbarTitle="Результаты: справка"
                rightbar={
                    <div>
                        <div className="help">
                            <div className="help__card">
                                Выможете начать новые измерения или сохранить текущий результат в историю лечения пациента и продолжить работу
                            </div>
                        </div>
                    </div>
                }
            />
        )
    }
}

export default Form.create({name: 'feedBack'})(ResultPage);