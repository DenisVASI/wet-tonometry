import React from "react";
import {Form, Icon, Select} from "antd";
import {Grid, Row, Col} from "react-flexbox-grid";
import {inject, observer} from "mobx-react";

import MobileSelectLayout from "../../layout/MobileSelectLayout";
import {Redirect} from "react-router";


const {Option} = Select;

@inject("patientStore", "mainLayoutStore")
@observer
class PatientForm extends React.Component {

    state = {
        redirect: false,
        redirectTo: null,
        showMedical: false,
        values: [],
        category: ["Не получает лечения", "Простагландины", "Бета блокаторы", "Адреномиметики", "Ингибиторы карбоангидразы", "М-холиномиметик", "Ретинопротекция"]
    };

    onChange = value => {


        if (value.indexOf("Не получает лечения") + 1) {
            this.setState({category: ["Не получает лечения"], values: ["Не получает лечения"]}, () => {
                this.props.form.setFieldsValue({
                    'medication': ["Не получает лечения"]
                });
            });
        } else {
            this.setState({
                category: ["Не получает лечения", "Простагландины", "Бета блокаторы", "Адреномиметики", "Ингибиторы карбоангидразы", "М-холиномиметик", "Ретинопротекция"],
                values: value
            }, () => {
                this.props.form.setFieldsValue({
                    'medication': value
                });
            })
        }


    };


    handleSubmit = e => {
        e.preventDefault();

        const {patientStore} = this.props;

        this.props.form.validateFields((err, values) => {
            if (!err) {
                patientStore.setPatientForm(values, () => {
                    this.setState({redirect: true, redirectTo: '/masses'});
                });
                console.log(values);
            }
        });
    };

    render() {

        const {getFieldDecorator} = this.props.form;

        if (this.state.redirect && this.state.redirectTo)
            return <Redirect to={this.state.redirectTo}/>;

        const {mainLayoutStore} = this.props;

        return (
            <Form onSubmit={this.handleSubmit} className="prepations_page__form">
                <Grid>
                    <Row>
                        <Col xs={12}>
                            <Form.Item label="Стадия глаукомы у пациента:">
                                {getFieldDecorator('stage', {
                                    rules: [{required: true, message: 'Укажите стадию глаукомы'}],
                                })(
                                    <Select placeholder="Выберите...">
                                        <Option value="0">Подозрение на глаукому</Option>
                                        <Option value="1">I Стадия глаукомы (начальная)</Option>
                                        <Option value="2">II Стадия глаукомы (развитая)</Option>
                                        <Option value="3">III - IV Стадия глаукомы (далеко зашедшая и
                                            терминальная)</Option>

                                    </Select>
                                )}
                            </Form.Item>

                            <div
                                className="form_mobile_container_hidden">
                                {mainLayoutStore.width < 500 && <div className="mobile-overlay" onClick={() => {
                                    this.setState({showMedical: true});
                                }}/>}

                                {this.state.showMedical && <MobileSelectLayout
                                    onCancel={() => {
                                        this.setState({showMedical: false});
                                    }}
                                    description="Какое лечение получает пациент ?"
                                    show={true}
                                    data={this.state.category}
                                    selected={this.state.values}
                                    onChange={(value) => {
                                        this.onChange(value);
                                    }}
                                    onSave={(value) => {
                                        console.log(value);
                                        this.onChange(value);
                                    }}
                                />}

                                <Form.Item label="Какое лечение получает пациент">
                                    {getFieldDecorator('medication', {
                                        rules: [{required: true, message: 'Укажите препарат'}],
                                        // initialValue: initialDrugs
                                    })(
                                        <Select
                                            mode="multiple"
                                            // value={this.state.value}
                                            placeholder="Выберите..."
                                            onChange={this.onChange}
                                        >

                                            {this.state.category.map((item, key) => {
                                                return <Option value={item} key={key}>{item}</Option>
                                            })}

                                        </Select>
                                    )}
                                </Form.Item>
                            </div>


                        </Col>
                    </Row>
                </Grid>
                <Form.Item>
                    <Grid>
                        <label className="ant-form-item-required"
                               title="Какое лечение получает пациент">Дата рождения</label>
                        <Row xs={12}>
                            <Col xs={4}>
                                <Form.Item>
                                    {getFieldDecorator('day', {
                                        rules: [{required: true, message: 'Укажите день'}],
                                    })(
                                        <Select
                                            placeholder="День"
                                        >
                                            {Array.from(Array(31).keys()).map(item => {
                                                return <Option value={item + 1} key={item + 1}>{item + 1}</Option>
                                            })}
                                        </Select>
                                    )}
                                </Form.Item>
                            </Col>
                            <Col xs={4}>
                                <Form.Item>
                                    {getFieldDecorator('mounth', {
                                        rules: [{required: true, message: 'Укажите месяц'}],
                                    })(
                                        <Select
                                            placeholder="Месяц"
                                        >
                                            <Option value={1}>Январь</Option>
                                            <Option value={2}>Фервраль</Option>
                                            <Option value={3}>Март</Option>
                                            <Option value={4}>Апрель</Option>
                                            <Option value={5}>Май</Option>
                                            <Option value={6}>Июнь</Option>
                                            <Option value={7}>Июль</Option>
                                            <Option value={8}>Август</Option>
                                            <Option value={9}>Сентябрь</Option>
                                            <Option value={10}>Октябрь</Option>
                                            <Option value={11}>Ноябрь</Option>
                                            <Option value={12}>Декабрь</Option>
                                        </Select>
                                    )}
                                </Form.Item>
                            </Col>
                            <Col xs={4}>
                                <Form.Item>
                                    {getFieldDecorator('year', {
                                        rules: [{required: true, message: 'Укажите год'}],
                                    })(
                                        <Select
                                            placeholder="Год"
                                        >
                                            {Array.from(Array(95).keys()).map(item => {
                                                return <Option value={item + 1920}
                                                               key={item + 1920}>{item + 1920}</Option>
                                            })}
                                        </Select>
                                    )}
                                </Form.Item>
                            </Col>
                        </Row>
                    </Grid>
                </Form.Item>
                <Grid>
                    <input type="submit" className="btn btn-blue" value="Сохранить"/>
                </Grid>
            </Form>
        );
    }
}

export default Form.create({name: "patient_form"})(PatientForm);