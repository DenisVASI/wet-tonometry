import React from "react";
import Main from "../../layout/Main";
import {Grid, Row, Col} from "react-flexbox-grid";
import PatientForm from "./PatientForm";
import TnPageHeader from "../../components/PageHeader";

class PreparationsPage extends React.Component {
    render() {

        const {width} = this.props;

        return (
            <Main
                stage={3}
                content={
                    <div className="preparations-page">
                        <div className="preparations-page__forms">
                            <Grid>
                                <Row>
                                    <Col xs={12} md={6}>
                                        <PatientForm width={width}/>
                                    </Col>
                                </Row>
                            </Grid>
                        </div>
                    </div>
                }
                rightbarTitle="О пациенте: справка"
                rightbar={
                    <div>
                        <div className="help">
                            <div className="help__card">
                                Введите следующую информацию о пациенте: стадия глаукомы, какое лечение получает
                                пациент и его
                                день рождения. Эта информация поможет нам более корректно подобрать лечение.
                            </div>
                            <div className="help__card">
                                Вся введенная вами информация будет обезличена, с целью соблюдения закона о
                                персональных данных.
                            </div>
                        </div>
                    </div>
                }/>
        );
    }
}

export default PreparationsPage;